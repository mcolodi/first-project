User.destroy_all

user1 = User.create(first_name: "Bob", last_name: "Foo", country: "MD", city: "Balti", age: 32)
user2 = User.create(first_name: "Alica", last_name: "Bar", country: "MD", city: "Chisinau", age: 23)
user3 = User.create(first_name: "Alan", last_name: "Test", country: "US", city: "Chishingtown", age: 42)
