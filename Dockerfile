# Use a base image with Ruby installed
FROM ruby:3.2.2

# Set the working directory inside the container
WORKDIR /app

# Copy the Gemfile and Gemfile.lock to the container
COPY user_api/Gemfile user_api/Gemfile.lock ./

# Install dependencies
RUN bundle install

# Copy the rest of the application code to the container
COPY ./user_api/ .
WORKDIR /app/user_api
